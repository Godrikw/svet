document.querySelector('.productPage__main_info-button').addEventListener('click', () => {
    document.querySelector('.modals').style.display = 'flex'
    document.querySelector('body').style.overflow = 'hidden'
})

document.querySelector('.modals__close').addEventListener('click', () => {
    document.querySelector('.modals').style.display = 'none'
    document.querySelector('body').style.overflow = 'auto'
})