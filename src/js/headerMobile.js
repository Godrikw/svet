document.querySelector('.header__burger').addEventListener('click', ()=> {
    document.querySelector('.header__menuMobile').style.display = 'flex'
    document.querySelector('body').style.overflow = 'hidden'
})


document.querySelector('.header__menuMobile_head-close').addEventListener('click', ()=> {
    document.querySelector('.header__menuMobile').style.display = 'none'
    document.querySelector('body').style.overflow = 'auto'
})

document.querySelector('.header__menuMobile__menu-item').addEventListener('click', (e)=> {
    document.querySelector('.header__menuMobile__menu-item').classList.toggle('header__menuMobile__menu-item-active')
})
